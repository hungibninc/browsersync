const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require("terser-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const BrowserSyncPlugin = require('browser-sync-webpack-plugin')

module.exports = {
  mode: 'development',
  entry: {
    index: './src/index.js',
  },
  output: {
    filename: '[name].lib.js',
    path: path.resolve(__dirname, 'dist'),
    // clean: true,
    // publicPath: '/',//  http://hungle.com
  },
  watch: true,
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Homepage',
      filename: 'index.html',
    }),
    new BrowserSyncPlugin({
      host: 'localhost',
      port: 3000,
      // server: { baseDir: ['dist'] },
      proxy: 'hosting.com',
      // injectChanges: true,
      open: false,
      files: [
        "./dist/*.css",
        "./dist/*.js",
        "./dist/*.html",
        "./dist/*.php",
      ],
    }),
    new MiniCssExtractPlugin(),
  ],
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
    ],
  },
  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all',
        },
      },
    },
    minimize: true,
    minimizer: [
      new TerserPlugin({
        exclude: /\/node_module/,
      }),
      new CssMinimizerPlugin(),
    ],
  },
};