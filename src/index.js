import _ from 'lodash';
import './styles.css';

function component() {
    const element = document.createElement('div');
    const btn = document.createElement('button');
    const br = document.createElement('br');
    
    btn.innerHTML = 'Click me and check the console!';
    element.innerHTML = _.join(['Hello1', 'webpackaaa'], ' ');
  
    element.appendChild(br);
    element.appendChild(btn);
  
    return element;
  }
  
  let element = component(); // Store the element to re-render on print.js changes
  document.body.appendChild(element);